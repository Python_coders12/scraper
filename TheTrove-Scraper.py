# A python 3 scraper for the website "The Trove"
import sys
from bs4 import BeautifulSoup as bs
import urllib.request as ur
import os
import time
from threading import Thread
import queue



# returns all the directories in the site and their coresponding urls
def searchDirs(site, url, folders):
    newfolders = []

    soup = bs(site, "html.parser")
    trs = soup.find_all('tr', {"class": "litem dir"})
    for tr in trs:
        link = tr.find('a')['href']
        folderName = tr.find('a').text
        if link != "../index.html":
            if url[-10:] == "index.html":
                url = url[:-10]

            # Create a listing with the name and the urls
            if folderName != "_Collections":
                folders.append((folderName, f"{url}{link}/"))
                newfolders.append(folderName)
    return folders, newfolders


# returns all the items in the site with their url
def searchItems(site, url):
    items = []

    soup = bs(site, "html.parser")
    trs = soup.find_all("tr", {"class": "litem file"})
    for tr in trs:
        link = f"{url}{tr.find('a')['href'][2:]}"
        fileName = tr.find('a').text
        items.append((fileName, link))
    return items


# creates all the folders in 1 directory
def createFolders(folders):
    for folder in folders:
        if not os.path.exists(folder):
            os.makedirs(folder)


def DownloadFiles(items):
    # Puts all the items in a query
    if items != []:
        for item in items:
            q.put(item)

    # Creates threads that are going to be downloading everyhting
    threads_list = []
    for i in range(15):
        t = Thread(target = DownloadFile, args = ([items]), daemon = True)
        threads_list.append(t)
        t.start()

    # Waits for those threads to finish, means all the directoties files have been downloaded
    for t in threads_list:
        t.join()


# Gets an item from the query and downloads it
def DownloadFile(items):
    try:
        while not q.empty():
            currentItem = q.get()
            if currentItem[0][-4:] == ".pdf" and not os.path.isfile(currentItem[0]):
                print(f'[Downloading]: {currentItem[0]}')
                try:
                    ur.urlretrieve(currentItem[1], currentItem[0])
                except Exception as e:
                    print(f"[FAILED] {currentItem[0]}, with {e}")

    except Exception:
        print(f"Failed to download: {currentItem[0]}")


if __name__ =="__main__":

    print("Welcome And Happy scraping")

    # create a folder in which everything will go in
    if not os.path.exists("Index"):
        os.makedirs("Index")
    os.chdir("Index")
    
    # the url of thetrove.net you want to scrape
    currentUrl = r"https://thetrove.net/Open%20Source/index.html"
    folders = []
    newfolders = []
    q = queue.Queue()

    userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 /(KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36"
    headers = ('User-Agent', userAgent)
    opener = ur.build_opener()
    opener.addheaders = [headers]
    ur.install_opener(opener)


    while True:
        # scan the page check for directories and files
        time.sleep(5)
        try:
            print(f"Going to {currentUrl}")
            resp = ur.urlopen(currentUrl).read()
            items = searchItems(resp, currentUrl)
            folders, newfolders = searchDirs(resp, currentUrl, folders)
            
            if items != []:
                DownloadFiles(items)
            
            # if there are folders, create them and go into 1 of them
            # make it's coresponding url the current url for scraping
            if newfolders != []:
                createFolders(newfolders)
                os.chdir(folders[-1][0])
                print(f"[Entering]: {folders[-1][0]}")
                currentUrl = folders[-1][1]
                folders.pop(-1)
                continue
            
            # if there are no more new directories go back and seach for the url/folder to scrape
            else:
                while True:
                    if os.path.exists("Index"):
                        sys.exit()

                    # If there is a target to go it

                    elif folders == []:
                        print("finished")
                        sys.exit()

                    elif os.path.exists(folders[-1][0]):
                        currentUrl = folders[-1][1]
                        os.chdir(folders[-1][0])
                        print(f"[Entering]: {folders[-1][0]}")
                        folders.pop(-1)
                        break

                    # else go back
                    else:
                        os.chdir("..")

        except Exception as e:
            print(f"failed {currentUrl}")
            folders.pop(-1)
            currentUrl = folders[-1][1]